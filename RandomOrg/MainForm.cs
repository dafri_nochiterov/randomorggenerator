﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using RandomOrg.Properties;

namespace RandomOrg
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > numericUpDown2.Value)
            {
                MessageBox.Show(Resources.AMoreThatBError, Resources.ErrorString, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }
            if (numericUpDown1.Value == numericUpDown2.Value)
            {
                label1.Text = numericUpDown1.Value + "";
                return;
            }
            numericUpDown1.Enabled = false;
            numericUpDown2.Enabled = false;
            button1.Enabled = false;
            label1.Visible = false;
            progressBar1.Visible = true;
            label1.Text =
                await
                    Task.Factory.StartNew(
                        () => AskRandomOrg((int) numericUpDown1.Value, (int) numericUpDown2.Value) + "");
            progressBar1.Visible = false;
            label1.Visible = true;
            button1.Enabled = true;
            numericUpDown1.Enabled = true;
            numericUpDown2.Enabled = true;
        }

        private void label1_TextChanged(object sender, EventArgs e)
        {
            label1.AutoEllipsis = true;
            label1.Left = (Width - label1.Width)/2;
            label1.Height = (Height - label1.Height)/2;
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            label1.Left = (Width - label1.Width)/2;
            label1.Height = (Height - label1.Height)/2;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            numericUpDown1.Maximum = 1000000000;
            numericUpDown2.Maximum = 1000000000;
            numericUpDown1.Minimum = -1000000000;
            numericUpDown2.Minimum = -1000000000;
        }

        private static int AskRandomOrg(int from, int to)
        {
            if (from >= to) throw new ArgumentException();
            const string format =
                "https://www.random.org/integers/?num=1&min={0}&max={1}&col=1&base=10&format=plain&rnd=new";
            var webClient = new WebClient();
            var readyLink = string.Format(format, from, to);
            var str = webClient.DownloadString(readyLink);
            int randomNumber;
            if (!int.TryParse(str, out randomNumber))
            {
                throw new Exception();
            }
            return randomNumber;
        }
    }
}